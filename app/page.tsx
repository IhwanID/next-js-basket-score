import Image from 'next/image'

 function Row({image, name, score, win, date} : { image: string, name: string, score: string, win: boolean, date: string}) {
  const formattedDate = new Date(date).toLocaleDateString('en-US', {
    day: '2-digit',
    month: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
  })
  return (
    <div className="flex border-b border-gray-200 justify-between px-4 py-2">
      <div className="flex">
        <Image
        src={image}
        alt="placeholder image"
        height={20}
        width={25}
        className='h-5 w-5'/>
        <p className="font-semibold ml-4">{name}</p>
      </div>
      <div className="flex text-right">
        <p className="text-gray-700">{score}</p>
        { score ? 
          <p className={`font-semibold ${win ? "text-green-700" : "text-red-700"} ml-2`}>{win ? "W" : "L"}</p> : <p className='text-gray-700'>{formattedDate}</p>}
      </div>
    </div>
  )
}

export default async function Home() {
  const res = await fetch('https://site.api.espn.com/apis/site/v2/sports/basketball/mens-college-basketball/teams/66/schedule');
  const data = await res.json();
  
  const events = data.events.map(
    (event: { competitions: { competitors: any[] }[] }) => {
    const competitors = event.competitions[0].competitors.map(
      (competitor) => {
       
        return {
          id: competitor.team.id,
          name: competitor.team.displayName,
          logo: competitor.team.logos[0].href,
          score: competitor.score,
          winner: competitor.winner,
        };
      }
    );

    const homeTeam = competitors.find((competitor) => competitor.id === "66");
    const awayTeam = competitors.find((competitor) => competitor.id !== "66");

    return {
      name: awayTeam?.name,
      logo: awayTeam?.logo,
      score: awayTeam?.score && `${awayTeam?.score.displayValue}-${homeTeam?.score.displayValue}`,
      winner: homeTeam?.winner,
      date: event.competitions[0].date,
    }
    }

  )



  return (
   <>
   <h2 className='font-semibold text-xl ml-4'>Schedule</h2>
   <h3 className='font-semibold text-gray-700 mb-2 ml-4'>Full</h3>
   <div>
   {events.map((event: {logo: string, name: string, score: string, winner: boolean, date: string}) =>  {
    return (<Row  key={event.name} image={event.logo} name={event.name} score={event.score} win={event.winner} date={event.date}/>)
   })}
   </div>
   </>
  )
}

